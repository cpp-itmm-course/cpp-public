from argparse import ArgumentParser
from pathlib import Path

TEST_FILE = lambda x: f'''
#include <catch2/catch_test_macros.hpp>
#include "{x}.h"

TEST_CASE(...) {
    
}

TEST_CASE(...) {
    
}
'''

CMAKE_FILE = lambda x: f'''
add_executable({x} test.cpp)
target_link_libraries({x} PRIVATE Catch2::Catch2WithMain)
'''

def update_cmakelists(block: Path, name: Path):
    with open(block / 'CMakeLists.txt', 'a') as f:
        f.write(f'add_subdirectory({name})\n')

def create_files(path: Path):
    path.mkdir(parents=True)
    with open(path / f'{path.name}.h', 'w') as f:
        pass
    with open(path / 'test.cpp', 'w') as f:
        f.write(TEST_FILE(path.name))
    with open(path / 'CMakeLists.txt', 'w') as f:
        f.write(CMAKE_FILE(path.name))
        

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('path')
    args = parser.parse_args()
    path = Path(args.path)
    create_files(path)
    update_cmakelists(path.parent, path.name)
