#include <catch2/catch_test_macros.hpp>
#include <string.h>
#include <iostream>

#include "file.h"

std::string Redirect(int n) {
    std::stringstream buffer;
    auto* old = std::cout.rdbuf(buffer.rdbuf());
    PrintConsole(n);
    std::cout.rdbuf(old);
    return std::move(buffer).str();
}

TEST_CASE("First test in my c++ program") {
    REQUIRE(Redirect(5) == "5\n");
    REQUIRE(Redirect(17) == "17\n");
    REQUIRE(Redirect(1324534537) == "1324534537\n");
}
