#include <random>

class RandomGenerator {
public:
    RandomGenerator(size_t seed): generator_(seed) {
    }

    std::string GetRandomString(size_t size, char from, char to) {
        std::uniform_int_distribution<char> dist(from, to);
        std::string result;
        result.reserve(size);
        for (int i = 0; i < size; ++i) {
            result.push_back(dist(generator_));
        }
        return result;
    }

    std::vector<int> GetRandomVector(size_t size, int from, int to) {
        std::uniform_int_distribution<int> dist(from, to);
        std::vector<int> v;
        v.reserve(size);
        for (int i = 0; i < size; ++i) {
            v.push_back(dist(generator_));
        }
        return v;
    }

private:
    std::mt19937 generator_;
};
