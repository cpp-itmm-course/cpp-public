#include <catch2/catch_test_macros.hpp>
#include "reverse-map.h"
#include <ranges>
#include <numeric>

TEST_CASE("Empty") {
    CHECK(ReverseMap({{1, 2}, {2, 2}, {3, 2}}, 100).empty());
    CHECK(ReverseMap({}, 100500).empty());
}

TEST_CASE("Simple") {
    {
        ArgType map{{1, 2}, {5, 6}};
        ResultType expected{{6, {5}}, {2, {1}}};
        CHECK(ReverseMap(map, 1) == expected);
    }
    {
        ArgType map{{5, 10}, {7, 10}, {3, 100}, {-100, 10}, {10, 100}};
        ResultType expected{{100, {3, 10}}};
        CHECK(ReverseMap(map, 2) == expected);
    }
    {
        ArgType map{{1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}};
        ResultType expected{{2, {1}}, {3, {2}}, {4, {3}}, {5, {4}}, {6, {5}}};
        CHECK(ReverseMap(map, 1) == expected);
    }
    {
        ArgType map{{1, 2}, {2, 2}, {3, 2}, {4, 2}, {5, 2}};
        ResultType expected{{2, {1, 2, 3, 4, 5}}};
        CHECK(ReverseMap(map, 9) == expected);
        CHECK(ReverseMap(map, 5) == expected);
        CHECK(ReverseMap(map, 1000).empty());
    }
}

TEST_CASE("Big") {
    ArgType map;
    ResultType expected;
    for (int i: std::ranges::iota_view{1, 10'000'000}) {
        map[i] = i / 5;
        expected[i / 5].push_back(i);
    }
    size_t x = 0;
    for (int i: std::ranges::iota_view{0, 4}) {
        CHECK(ReverseMap(map, x) == expected);
        auto it = expected.find(i);
        x = std::accumulate(it->second.begin(), it->second.end(), 0);
        expected.erase(it);
    }
}
