#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

#include <array>
#include <ranges>

#include "freq.h"

using Catch::Matchers::Equals;
using VectorType = std::vector<std::pair<std::string, size_t>>;

TEST_CASE("Empty") {
    CHECK(MostFrequency({}, 5).empty());
    CHECK(MostFrequency({}, 0).empty());
    CHECK(MostFrequency({}, 7).empty());
}

TEST_CASE("Simple") {
    CHECK_THAT(MostFrequency({"c++", "python", "c++"}, 1), Equals(VectorType{{"c++", 1}}));
    CHECK_THAT(MostFrequency({"c++", "c++", "c++", "python", "python"}, 2),
               Equals(VectorType{{"c++", 3}, {"python", 2}}));
    CHECK_THAT(MostFrequency({"python", "c++", "c++", "python", "c++"}, 2),
               Equals(VectorType{{"c++", 3}, {"python", 2}}));
    CHECK_THAT(MostFrequency({"a", "b", "c", "c", "b", "a"}, 2),
               Equals(VectorType{{"a", 2}, {"b", 2}}));
    CHECK_THAT(MostFrequency({"b", "b", "a", "a", "a", "a", "b", "b", "b", "b", "b", "a", "a"}, 1),
               Equals(VectorType{{"b", 7}}));
    CHECK_THAT(MostFrequency({"aa", "aab", "aa", "bb", "python", "c++"}, 5),
               Equals(VectorType{{"aa", 2}, {"aab", 1}, {"bb", 1}, {"c++", 1}, {"python", 1}}));
    CHECK_THAT(MostFrequency({"hello, c++", "hello, c++", "hello, c++", "hello, c++", "hello, c++"}, 1),
               Equals(VectorType{{"hello, c++", 5}}));
    CHECK_THAT(MostFrequency({"a", "a", "c", "c", "b", "b"}, 3),
               Equals(VectorType{{"a", 2}, {"b", 2}, {"c", 2}}));
}

TEST_CASE("Big") {
    std::array symbols = {'c', 'b', 'a'};
    std::vector<std::string> v;
    for (auto s1: symbols) {
        for (auto s2: symbols) {
            for (auto s3: symbols) {
                v.push_back({s1, s2, s2});
            }
        }
    }
    std::vector<std::string> v_repeater = v;
    for (auto i: std::ranges::iota_view{1, 10}) {
        v_repeater.insert(v_repeater.end(), v.begin(), v.end());
    }
    CHECK_THAT(MostFrequency(v_repeater, 3), Equals(VectorType{{"aaa", 10}, {"aab", 10}, {"aac", 10}}));
}
