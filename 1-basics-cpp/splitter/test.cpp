#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <catch2/matchers/catch_matchers_range_equals.hpp>

#include <catch2/generators/catch_generators_adapters.hpp>
#include <catch2/generators/catch_generators_random.hpp>


#include "split.h"
#include "generator.h"

#include <ranges>

using Catch::Matchers::Equals;

TEST_CASE("Simple") {
    CHECK_THAT(Splitter("hello", '.'), Equals(std::vector<std::string>{"hello"}));
    CHECK_THAT(Splitter("hello", 'o'), Equals(std::vector<std::string>{"hell", ""}));
    CHECK_THAT(Splitter("hello", 'h'), Equals(std::vector<std::string>{"", "ello"}));
    CHECK_THAT(Splitter("path/to/directory", '/'),
               Equals(std::vector<std::string>{"path", "to", "directory"}));
    CHECK_THAT(Splitter("hello world i am c++ progger", ' '),
               Equals(std::vector<std::string>{"hello", "world", "i", "am", "c++", "progger"}));
    CHECK_THAT(Splitter("hhh?h", 'h'), Equals(std::vector<std::string>{"", "", "", "?", ""}));
    CHECK_THAT(Splitter("google.com", '.'), Equals(std::vector<std::string>{"google", "com"}));
}

TEST_CASE("Random") {
    auto seed = GENERATE(take(10, random(0u, 1'000'000'000u)));
    auto string = RandomGenerator{seed}.GetRandomString(100'000, '1', '9');
    std::vector<std::string> result;
    for (auto subrange: std::views::split(string, '5')) {
        result.emplace_back(subrange.begin(), subrange.end());
    }
    CHECK_THAT(Splitter(string, '5'), Equals(result));
}
