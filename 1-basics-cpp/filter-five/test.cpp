#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

#include <catch2/generators/catch_generators_random.hpp>
#include <catch2/generators/catch_generators_adapters.hpp>


#include "filter.h"
#include "generator.h"

using Catch::Matchers::Equals;

TEST_CASE("Empty") {
    std::vector<int> v;
    FilterFive(&v);
    CHECK(v.empty());

    std::vector vv{1, -1};
    FilterFive(&vv);
    CHECK(vv.empty());

    std::vector vvv{-9, -8};
    FilterFive(&vvv);
    CHECK(vvv.empty());
}


TEST_CASE("Simple") {
    {
        std::vector v = {1, 2, 3, 4, 5, 10, 3, 15, 16};
        FilterFive(&v);
        CHECK_THAT(v, Equals(std::vector{5, 10, 15}));
    }
    {
        std::vector v = {1, 2};
        FilterFive(&v);
        CHECK_THAT(v, Equals(std::vector<int>{}));
    }
    {
        std::vector v = {100, 200, 3, 7, 300, 45, 6, 50, 55, 59, 60, 0};
        FilterFive(&v);
        CHECK_THAT(v, Equals(std::vector<int>{100, 200, 300, 45, 50, 55, 60, 0}));
    }
    {
        std::vector v = {5, 10, 3, 10};
        FilterFive(&v);
        CHECK_THAT(v, Equals(std::vector<int>{5, 10, 10}));
    }
}

TEST_CASE("Random") {
    auto seed = GENERATE(take(10, random(0u, 100'000u)));
    auto test = RandomGenerator{seed}.GetRandomVector(500'000, -1'000'000, 1'000'000);
    auto expected = test;
    std::erase_if(expected, [](auto x) { return x % 5; });
    FilterFive(&test);
    CHECK(test == expected);
}
