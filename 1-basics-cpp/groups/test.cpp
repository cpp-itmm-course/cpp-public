#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <catch2/matchers/catch_matchers_range_equals.hpp>

#include <ranges>

#include "groups.h"

using Catch::Matchers::Equals;
using Catch::Matchers::RangeEquals;

TEST_CASE("Empty") {
    CHECK(GroupElements({}).empty());
}

TEST_CASE("Simple") {
    CHECK_THAT(GroupElements({1}), Equals(std::vector<std::pair<int, int>>{{1, 1}}));
    CHECK_THAT(GroupElements({1, 1, 1}), Equals(std::vector<std::pair<int, int>>{{3, 1}}));
    CHECK_THAT(GroupElements({0, 1, 1, 0, 0, 1}),
               Equals(std::vector<std::pair<int, int>>{{1, 0}, {2, 1}, {2, 0}, {1, 1}}));
    CHECK_THAT(GroupElements({-5, 3, -7, -7, 3, 3, -5, -5, -5, -5, -5}),
               Equals(std::vector<std::pair<int, int>>{{1, -5}, {1, 3}, {2, -7}, {2, 3}, {5, -5}}));
    CHECK_THAT(GroupElements({1, 2, 3}),
               Equals(std::vector<std::pair<int, int>>{{1, 1}, {1, 2}, {1, 3}}));
}

TEST_CASE("Big") {
    auto from = -1'000'000;
    auto to = 1'000'000;
    auto range = std::views::iota(from, to);
    auto transformer = range | std::views::transform([](int x) -> std::pair<int, int> {
        return {1, x};
    });
    CHECK_THAT(GroupElements(std::vector<int>(range.begin(), range.end())),
               RangeEquals(std::vector<std::pair<int, int>>(transformer.begin(), transformer.end())));
}
